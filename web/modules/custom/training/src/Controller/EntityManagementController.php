<?php

namespace Drupal\training\Controller;

use Drupal\Core\Controller\ControllerBase;

/**
 * Class EntityManagementController.
 */
class EntityManagementController extends ControllerBase {

  /**
   * Dump entites.
   */
  public function dump() {

    /**
     * $entity_type_id = 'node';
     * $entity_type_id = 'taxonomy_term';
     * $entity_type_id = 'block_content';
     * $entity_type_id = 'user';
     * $entity_type_id = 'contact_message';
     */

    /**
     * TODO: Create content 'article' with devel_generate before trying this controller.
     * TODO: Option: Add a simple text field to the article node type.
     * Check the cardinality configuration of the fields.
     * Default : field_image : 0..1 -- field_tags : 0..*
     */

    $nid = 41; // Set the nid of an article.
    $entity_type_id = 'node';
    $node_storage_interface = $this->entityTypeManager()->getStorage($entity_type_id);
    $entity = $node_storage_interface->load($nid);


    dump('----------------------------------------------');
    dump('##             DUMP FULL ENTITY             ##');
    dump('----------------------------------------------');
    dump($entity);


    dump('-----------------------------------------------');
    dump('##              DUMP FIELD ITEM              ##');
    dump('-----------------------------------------------');
    /**
     * Abstract class ContentEntityBase implements the magic method __get() for getting object properties.
     * @see https://api.drupal.org/api/drupal/core%21lib%21Drupal%21Core%21Entity%21ContentEntityBase.php/class/ContentEntityBase/8.8.x
     *
     * You can use the FieldableEntityInterface for getting the field item object.
     * @see https://api.drupal.org/api/drupal/core%21lib%21Drupal%21Core%21Entity%21FieldableEntityInterface.php/interface/FieldableEntityInterface/8.7.x
     */

    dump($entity->body); // use magic-method, return null if an invalid field name is given.
    dump($entity->get('body')); // @throws \InvalidArgumentException


    dump('-----------------------------------------------');
    dump('##             TITLE INFORMATION             ##');
    dump('-----------------------------------------------');
    /**
     * Use the ContentEntityInterface for getting entities commons datas.
     * @see https://api.drupal.org/api/drupal/core%21lib%21Drupal%21Core%21Entity%21ContentEntityInterface.php/interface/ContentEntityInterface/8.7.x
     */

    dump($entity->title->value);
    dump($entity->getTitle());


    /**
     * TODO : Implements a common field : text, boolean, int...
     */
    // dump('----------------------------------------------');
    // dump('## COMMON FIELD ##');
    // dump('----------------------------------------------');
    /**
     * Class FieldItemList implements the magic method __get() for getting object properties.
     * @see https://api.drupal.org/api/drupal/core%21lib%21Drupal%21Core%21Field%21FieldItemList.php/class/FieldItemList/8.2.x
     *
     * You can use the FieldItemListInterface for getting the field item object.
     * @see https://api.drupal.org/api/drupal/core%21lib%21Drupal%21Core%21Field%21FieldItemListInterface.php/interface/FieldItemListInterface/8.2.x
     */

    // dump($entity->[field_name]->value);
    // dump($entity->get('[field_name]')->value);
    // dump($entity->get('[field_name]')->getValue());


    dump('-----------------------------------------------');
    dump('##           ENTITY REFERENCE FIED           ##');
    dump('-----------------------------------------------');
    /**
    * EntityReferenceFieldItemListInterface interface.
    * @see https://api.drupal.org/api/drupal/core%21lib%21Drupal%21Core%21Field%21EntityReferenceFieldItemListInterface.php/interface/EntityReferenceFieldItemListInterface/8.2.x
    */
    dump('1/ FIELD FILE');
    dump('----------------------------------------------');
    /**
     * Du to the cardinality configuration, the field contains only one reference.
     */
    dump('-- Using magic method');
    dump($entity->field_image->target_id);    // You get the first reference.
    dump($entity->get('field_image')->alt);   // You get the first reference.

    dump('-- Using getters');
    dump($entity->get('field_image')->getValue());            // You get all the references.
    dump($entity->get('field_image')->referencedEntities());  // You get all the references.


    dump('----------------------------------------------');
    dump('2/ TAXONOMY REFERENCE FIELD');
    dump('----------------------------------------------');
    /**
     * Due to the cardinality configuration, the field contains several references.
     */
    dump('-- Using magic method');
    dump($entity->field_tags->target_id);         // You get the first reference.
    dump($entity->get('field_tags')->target_id);  // You get the first reference.

    dump('-- Using getters');
    dump($entity->get('field_tags')->first()->getValue());    // You get the first reference.
    dump($entity->get('field_tags')->getValue());             // You get all the references.
    dump($entity->get('field_tags')->referencedEntities());   // You get all the entities.

    return;
  }

}
