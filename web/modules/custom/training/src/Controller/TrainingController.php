<?php

namespace Drupal\training\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\user\Entity\User;


/**
 * Class TrainingController.
 */
class TrainingController extends ControllerBase {


  /**
   * Simple controller render a markup.
   */
  public function render() {
    return [
      '#markup' => $this->t('Hello world'),
    ];
  }

  /**
   * Controller can use argument.
   */
  public function renderArgument($foo, $bar) {
    return [
      '#markup' => '<li>$foo='  . $foo . '</li><li>$bar=' . $bar. '</li>',
      '#prefix' => '<ul>',
      '#suffix' => '</ul>',
    ];
  }

  /**
   * Controller can access to the entire entity.
   */
  public function renderEntity(User $user) {
    return [
      '#markup' => '<p>' . $this->t('Hello world') . ' ' . $user->getAccountName() . '</p>',
    ];
  }

  public function getTitle() {
    return 'Welcome ' . $this->currentUser()->getAccountName();
  }
}
