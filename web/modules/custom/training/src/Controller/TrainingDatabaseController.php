<?php

namespace Drupal\training\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Database\Driver\mysql\Connection;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Class TrainingDatabaseController.
 */
class TrainingDatabaseController extends ControllerBase {

  /**
   * @var \Drupal\Core\Database\Driver\mysql\Connection
   */
  protected $database;

  /**
   * {@inheritdoc}
   */
  public function __construct(Connection $database) {
    $this->database = $database;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
        $container->get('database'),
    );
  }

  /**
   * Database queries examples.
   */
  public function readDatabase() {

    // Requete faite directement sur les tables.
    $query = $this->database->select('users', 'u')
      ->fields('u', ['uid', 'uuid']);
    $result = $query->execute();

    dump('Traitement des reponse dans une boucle');
    foreach ($result as $record) {
      dump($record);
    }

    dump('fetchField');
    dump($query->execute()->fetchField());
    dump('fetchAll');
    dump($query->execute()->fetchAll());
    dump('fetchCol');
    dump($query->execute()->fetchCol());
    dump('fetchAssoc');
    dump($query->execute()->fetchAssoc());
    dump('fetchAllAssoc');
    dump($query->execute()->fetchAllAssoc('uid'));
    dump('fetchAllKeyed');
    dump($query->execute()->fetchAllKeyed());
  }

  /**
   * EntityQuery's examples.
   */
  public function readEntity() {

    $entity_type = 'user';
    // $entity_type = 'node';
    // $entity_type = 'block_content';
    // $entity_type = 'taxonomy_term';
    // $entity_type = 'contact_form';
    // $entity_type = 'file';
    $uids = $this->entityTypeManager()
      ->getStorage($entity_type)
      ->getQuery()
      ->execute();
    $count_users = count($uids);

    $nids = $this->entityTypeManager()
      ->getStorage('node')
      ->getQuery()
      ->condition('type', 'article', '=')
      ->execute();
    $count_articles = count($nids);

    $list = [
      'Nombre utilisateur : ' . $count_users,
      'Nombre d\'articles : ' . $count_articles,
    ];

    return [
      '#theme' => 'item_list',
      '#items' => $list,
    ];
  }
}
