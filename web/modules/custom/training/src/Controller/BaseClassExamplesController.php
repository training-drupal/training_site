<?php

namespace Drupal\training\Controller;

use Drupal\Core\Controller\ControllerBase;

/**
 * Class BaseClassExamplesController.
 */
class BaseClassExamplesController extends ControllerBase {

  /**
   *
   */
  public function services() {

    /**
     * Accès à l'utilisateur courant.
     * @return \Drupal\Core\Session\AccountInterface
    **/
    $current_user = $this->currentUser();
    dump($current_user);

    /**
     * Accès à l'entity type Mananger.
     * Permet de charger une entité.
     * Faire une requête sur une entité.
     * @see https://api.drupal.org/api/drupal/core%21lib%21Drupal%21Core%21Entity%21EntityTypeManagerInterface.php/interface/EntityTypeManagerInterface/8.7.x
     * @return \Drupal\Core\Entity\EntityTypeManagerInterface
    **/
    $this->entityTypeManager();

    // Pour accèder à un type d'entité spécifique.
    $entity_type_id = 'user';
    // $entity_type_id = 'node';
    // $entity_type_id = 'taxonomy_term';
    // $entity_type_id = 'contact_message';
    $user_storage = $this->entityTypeManager()->getStorage($entity_type_id);
    $user_by_load = $user_storage->load(1);
    dump($user_by_load);

    // Faire une requête sur les entités.
    $user_id = $this->entityTypeManager()->getStorage($entity_type_id)->getQuery()
      ->condition('uid', 1, '=')
      ->execute();
    dump($user_id);

    /**
     * Accès à constructeur de formulaire.
     * Pour construire des formulaires personnalisés.
     * @see https://api.drupal.org/api/drupal/core%21lib%21Drupal%21Core%21Form%21FormBuilderInterface.php/8.2.x
     */
    $this->formBuilder();

    /**
     * Accès à l'entity form builder.
     * pour construire des formulaire de type entité.
     * Ex : Les formulaires du backoffice
     * @see https://api.drupal.org/api/drupal/core%21lib%21Drupal%21Core%21Entity%21EntityFormBuilderInterface.php/interface/EntityFormBuilderInterface/8.2.x
     * @return \Drupal\Core\Entity\EntityFormBuilderInterface
     */
    $this->entityFormBuilder();

    // On récupére l'entity à partir de l'entity type manager.
    // On obtient l'entité $form, paramètre de la méthode de l'interface.
    $entity_type_id = 'contact_message';
    $form_entity = $this->entityTypeManager()
      ->getStorage($entity_type_id)
      ->create([
        'contact_form' => 'feedback',
      ]);

    $form = $this->entityFormBuilder->getForm($form_entity);
    dump($form);

    /**
     * Pour effectuer des redirection.
    * @return \Symfony\Component\HttpFoundation\RedirectResponse
    *   A redirect response object that may be returned by the controller.
    */
    $redirect = $this->redirect('entity.user.canonical', ['user' => 1]);
    dump($redirect);

    /**
     * Accès au paramêtre de configuration du système.
     * @param string $name configuration file name
     * @see https://api.drupal.org/api/drupal/core%21lib%21Drupal%21Core%21Config%21Config.php/class/Config/8.2.x
     * @return \Drupal\Core\Config\Config
    **/
    $name = 'system.site';
    $config = $this->config($name)->getRawData();
    dump($config);

    /**
     * Pour logger des messages, visible en backoffice.
     * @param string $name le nom du channel
     * @see https://api.drupal.org/api/drupal/vendor!psr!log!Psr!Log!LoggerInterface.php/interface/LoggerInterface/8.8.x
    **/
    $this->getLogger($this->t('My channel'))->notice($this->t('My message'));

    return [
      '#markup' => 'Hello World \o/',
    ];

    // Décommenter pour tester la redirection.
    // return $redirect;.
  }

}
