<?php

namespace Drupal\training\Controller;

use Drupal\Core\Controller\ControllerBase;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\RequestStack;

/**
 * Class TrainingFormController.
 */
class TrainingFormController extends ControllerBase {

  /**
   * @var \Symfony\Component\HttpFoundation\RequestStack
   */
  protected $requestManager;

  /**
   * {@inheritdoc}
   */
  public function __construct(RequestStack $request_manager) {
    $this->requestManager = $request_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
        $container->get('request_stack'),
    );
  }

  /**
   * You can access to the query params.
   */
  public function renderFormResult() {

    $message = $this->t('My redirection page');
    $params = $this->requestManager->getCurrentRequest()->query->all();

    if (array_key_exists('input', $params)) {
      $message =  $this->t('My input :') .  $params['input'];
    }

    return [
      '#markup' => '<p>' . $message . '</p>'
    ];
  }
}
