<?php

namespace Drupal\training\Plugin\Block\TrainingBlock;

use Drupal\Core\Block\BlockBase;

/**
 * Class TrainingBlock.
 *
 * @Block(
 *  id = "training_block",
 *  admin_label = @Translation("Training block"),
 * )
 */
class TrainingBlock extends BlockBase {

  /**
   * {@inheritdoc}
   */
  public function build() {
    return [
      '#markup' => $this->t('Hello block \o/'),
    ];
  }
}
