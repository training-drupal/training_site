<?php

namespace Drupal\training\Form\Multistep;

use Drupal\Core\Form\FormStateInterface;

/**
 * Class MultiStepOneForm.
 */
class MultiStepOneForm extends MultiStepBaseForm {

  /**
   * {@inheritdoc}.
   */
  public function getFormId() {
    return 'multistep_form_one';
  }

  /**
   * {@inheritdoc}.
   */
  public function buildForm(array $form, FormStateInterface $form_state) {

    $form = parent::buildForm($form, $form_state);

    $form['name'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Your name'),
      '#default_value' => $this->store->get('name') ? $this->store->get('name') : '',
    ];

    $form['actions']['submit']['#value'] = $this->t('Next');
    $form['actions']['reverse']['#access'] = FALSE;
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $this->store->set('email', $form_state->getValue('email'));
    $form_state->setRedirect('training.multistep_two');
  }

}
