<?php

namespace Drupal\training\Form\Multistep;

use Drupal\Core\Form\FormStateInterface;

/**
 * Class MultiStepTwoForm.
 */
class MultiStepTwoForm extends MultiStepBaseForm {

  /**
   * {@inheritdoc}.
   */
  public function getFormId() {
    return 'multistep_form_two';
  }

  /**
   * {@inheritdoc}.
   */
  public function buildForm(array $form, FormStateInterface $form_state) {

    $form = parent::buildForm($form, $form_state);

    $form['email'] = [
      '#type' => 'email',
      '#title' => $this->t('Your email address'),
      '#default_value' => $this->store->get('email') ? $this->store->get('email') : '',
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $this->store->set('email', $form_state->getValue('email'));

    $triggering = $form_state->getTriggeringElement();
    $action = $triggering['#id'];
    switch ($action) {
      case 'edit-submit':
        parent::saveData();
        $form_state->setRedirect('training.redirection');
        break;

      case 'edit-reverse':
        $form_state->setRedirect('training.multistep_one');
        break;

      default:
        $form_state->setRedirect('training.multistep_one');
    }
  }

}
