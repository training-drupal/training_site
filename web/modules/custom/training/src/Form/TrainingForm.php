<?php

namespace Drupal\training\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Class TrainingForm.
 */
class TrainingForm extends FormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'training_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {

    $form['description'] = [
      '#type' => 'item',
      '#markup' => $this->t('This example shows the imlementation of a simple form'),
    ];

    $form['input_text'] = [
      '#type' => 'textfield',
      '#title' => $this->t('My object'),
      '#description' => $this->t('The description of the input'),
      '#size' => 60,
      '#maxlength' => 128,
    ];

    $form['checkbox'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('You can check this'),
      '#description' => $this->t('The description of the checbox\'s purpose'),
    ];

    $form['actions']['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Submit'),
      '#description' => $this->t('Submit'),
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    $input = $form_state->getValue('input_text');
    if (preg_match('/\d/', $input)) {
      $form_state->setErrorByName('input_text', $this->t('Numbers are not allowed !'));
    }
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $parameters = [
      'input' => $form_state->getValue('input_text'),
    ];
    $this->logger('Channel')->notice($this->t('My message'));
    $form_state->setRedirect('training.redirection', $parameters);
  }
}
