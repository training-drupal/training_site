<?php

namespace Drupal\sam\Controller\TpController;

use Drupal\Core\Controller\ControllerBase;

/**
 * Class Lv1Controller.
 */
class Lv1Controller extends ControllerBase {

  public function render() {

    // get datas from current user
    $username = $this->currentUser()->getAccountName();
    $mail = $this->currentUser()->getEmail();
    $last_connexion = $this->currentUser()->getLastAccessedTime() != 0 ?
      \date('d/m/Y', $this->currentUser()->getLastAccessedTime()) : $this->t('never');

      // get datas from entity
    $user = $this->entityTypeManager()
      ->getStorage('user')
      ->load($this->currentUser()->id());
    $created = date('d/m/Y', $user->getCreatedTime());

    $list = [
      $this->t('Username') . ' : ' . $username,
      $this->t('Email') . ' : ' . $mail,
      $this->t('Created') . ' : ' . $created,
      $this->t('Last connexion') . ' : ' . $last_connexion,
    ];

    return [
      '#theme' => 'item_list',
      '#list_type' => 'ul',
      '#items' => $list,
      '#prefix' => '<h1>' . $this->t('About me') . '</h1>',
    ];
  }

}
