<?php

namespace Drupal\sam\Controller\TpController;

use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Datetime\DateFormatter;
use Drupal\Core\Render\RendererInterface;
use Drupal\user\Entity\User;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Class Lv3Controller.
 */
class Lv3Controller extends ControllerBase {

  /**
   * @var \Drupal\Core\Datetime\DateFormatter
   */
  protected $dateFormatter;

  /**
 * @var \Drupal\Core\Render\RendererInterface
 */
 protected $renderer;

  /**
   * {@inheritdoc}
   */
  public function __construct(DateFormatter $date_formatter, RendererInterface $renderer) {
    $this->dateFormatter = $date_formatter;
    $this->renderer = $renderer;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
        $container->get('date.formatter'),
        $container->get('renderer'),
    );
  }

  public function render(User $user) {

    $level = '';
    if ($user->hasField('field_level')) {
      $field_level = $user->get('field_level')->first()->getValue();
      if(!empty($field_level)) {
        $term_level = $this->entityTypeManager()
          ->getStorage('taxonomy_term')
          ->load($field_level['target_id']);
        if (!\is_null($term_level)) {
          $level = $term_level->getName();
        }
      }
    }

    $subscriptions = [t('Not specified')];
    if ($user->hasField('field_activities')) {
      $field_activities = $user->get('field_activities')->getValue();
      if(!empty($field_activities)) {
        $nids = \array_column($field_activities, 'target_id');
        $nodes = $this->entityTypeManager()
          ->getStorage('node')
          ->loadMultiple($nids);
        foreach ($nodes as $node) {
          $list[] = $node->getTitle();
        }
        $subscriptions = [
          '#theme' => 'item_list',
          '#list_type' => 'ul',
          '#items' => $list,
        ];
      }
    }

    $header = [
      $this->t('Username'),
      $this->t('Email'),
      $this->t('Created since'),
      $this->t('Last connexion'),
      $this->t('Level'),
      $this->t('Sport'),
    ];

    $output[] = [
      $user->getAccountName(),
      $user->getEmail(),
      $this->dateFormatter->formatTimeDiffSince($user->getCreatedTime()),
      $user->getLastAccessedTime() != 0 ?
        \date('d/m/Y', $user->getLastAccessedTime()) : $this->t('never'),
      $level,
      $this->renderer->render($subscriptions),
    ];

    $this->messenger()->addWarning($this->t('Confidentials informations'));

    return [
      '#type' => 'table',
      '#header' => $header,
      '#rows' => $output,
    ];
  }

  /**
   * TP Controller - level 3.
   */
  public function titleCallback(User $user) {
    return \sprintf('About %s', $user->getAccountName());
  }

}
