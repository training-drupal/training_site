<?php

namespace Drupal\sam\Controller\TpForm;

use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\TempStore\PrivateTempStoreFactory;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\RequestStack;

/**
 * Class FormRedirectionController.
 */
class FormRedirectionController extends ControllerBase {

  /**
   * @var \Symfony\Component\HttpFoundation\RequestStack
   */
  protected $requestManager;

  /**
   * @var \Drupal\Core\TempStore\PrivateTempStoreFactory;
   */
  protected $tempStoreFactory;

  /**
   * @var \Drupal\user\PrivateTempStore
   */
  protected $store;

  /**
   * {@inheritdoc}
   */
  public function __construct(RequestStack $request_manager, PrivateTempStoreFactory $temp_store_factory) {
    $this->requestManager = $request_manager;
    $this->tempStoreFactory = $temp_store_factory;
    $this->store = $this->tempStoreFactory->get('sam');
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
        $container->get('request_stack'),
        $container->get('tempstore.private'),
    );
  }

  /**
   * TP formulaire : Level 2.
   */
  public function formRedirect() {

    $params = $this->requestManager->getCurrentRequest()->query->all();

    $name = \array_key_exists('name', $params) ? $params['name'] : '';
    $mail  = \array_key_exists('mail', $params) ? $params['mail'] : '';
    $sport = \array_key_exists('sport', $params) ? $params['sport'] : '';

    $list = [
      $this->t('name') . ': ' . $name,
      $this->t('Email') . ': ' . $mail,
      $this->t('Sport') . ': ' . $sport,
    ];

    return [
      '#theme' => 'item_list',
      '#list_type' => 'ul',
      '#items' => $list,
      '#prefix' => '<h1>' . $this->t('Thank you for your subscription') . '</h1>',
    ];
  }

  /**
   * TP formulaire : Level 2.
   */
  public function formRedirectUsingTempStore() {

    $list = [];
    $datas = $this->store->get('newsletters_subscription_form_data');
    $this->store->delete('newsletters_subscription_form_data');

    if (!\is_null($datas)) {
      $name = \array_key_exists('name', $datas) ? $datas['name'] : '';
      $mail  = \array_key_exists('mail', $datas) ? $datas['mail'] : '';
      $sport = \array_key_exists('sport', $datas) ? $datas['sport'] : '';

      $list = [
        $this->t('name') . ': ' . $name,
        $this->t('Email') . ': ' . $mail,
        $this->t('Sport') . ': ' . $sport,
      ];
    }

    return [
      '#theme' => 'item_list',
      '#list_type' => 'ul',
      '#items' => $list,
      '#prefix' => '<h1>' . $this->t('Thank you for your subscription') . '</h1>',
    ];
  }

  /**
   * TP formulaire : Level 3.
   */
  public function redirectionNewsletter() {
    return [
      '#markup' => 'Inscription confirmée',
    ];
  }

}
