<?php

namespace Drupal\sam\Form\TpForm;

use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\TempStore\PrivateTempStoreFactory;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Class Lv2NewsletterSubscribeForm.
 */
class Lv2NewsletterSubscribeForm extends FormBase {

  /**
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * @var \Drupal\Core\TempStore\PrivateTempStoreFactory
   */
  protected $tempStoreFactory;

  /**
   * @var \Drupal\user\PrivateTempStore
   */
  protected $store;

  /**
   * {@inheritdoc}
   */
  public function __construct(EntityTypeManagerInterface $entity_type_manager,
    PrivateTempStoreFactory $temp_store_factory
  ) {
    $this->entityTypeManager = $entity_type_manager;
    $this->tempStoreFactory = $temp_store_factory;
    $this->store = $this->tempStoreFactory->get('sam');
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('entity_type.manager'),
      $container->get('tempstore.private'),
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'lv2_newsletter_subscribe_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {

    if (!$this->currentUser()->isAnonymous()) {
      $account = $this->entityTypeManager->getStorage('user')->load($this->currentUser()->id());
      if($account->hasField('field_newsletter')) {
        $field_newsletter = $account->get('field_newsletter')->first()->getValue();
        if (!empty($field_newsletter)) {
          if ($field_newsletter['value']) {
            $this->messenger()->addStatus($this->t('You are already registred'));
            return $this->redirect('entity.user.edit_form', ['user' => $this->currentUser()->id()]);
          }
        }
      }
    }

    $form['name'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Your name'),
      '#required' =>TRUE,
    ];

    $form['mail'] = [
      '#type' => 'email',
      '#title' => $this->t('Your email address'),
      '#required' =>TRUE,
    ];

    if (!$this->currentUser()->isAnonymous()) {
      $form['name']['#default_value'] = $account->getAccountName();
      $form['mail']['#default_value'] = $account->getEmail();
    }

    // Get all terms from vocabulary and create an options tab for select input.
    $options = [];
    $terms = $this->entityTypeManager->getStorage('taxonomy_term')->loadTree('sport');
    if (!empty($terms)) {
      foreach ($terms as $term) {
        $term_name = $term->name;
        $options[$term->tid] = $term_name;
      }
    }

    $form['sport'] = [
      '#type' => 'select',
      '#title' => $this->t('Center of interest'),
      '#required' => TRUE,
      '#options' => $options,
      '#empty_option' => $this->t('-- Select a sport --'),
    ];

    $form['postal_code'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Your postal code'),
      '#maxlength' => 5,
    ];

    $form['locality'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Your locality'),
      '#states' => [
        'invisible' => [
          ':input[name="postal_code"]' => ['filled' => FALSE],
        ],
      ],
    ];

    $form['actions']['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Submit'),
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {

    $postal_code = $form_state->getValue('postal_code');
    if ($postal_code != '') {
      if (\strlen($postal_code) < 5) {
        $form_state->setErrorByName('postal_code', $this->t('Enter a valid postal code !'));
      }
      if (!\preg_match('/^[0-9]*$/', $postal_code)) {
        $form_state->setErrorByName('postal_code', $this->t('Only number are allowed !'));
      }

      $locality = $form_state->getValue('locality');

      if ($locality == '') {
        $form_state->setErrorByName('locality', $this->t('You must provide your locality'));
      }
      if (\preg_match('/\d+/', $locality)) {
        $form_state->setErrorByName('locality', $this->t('Only character are allowed !'));
      }
    }
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {

    // Get all datas from form.
    $values = $form_state->getValues();
    $term_sport = $this->entityTypeManager->getStorage('taxonomy_term')->load($values['sport']);

    $parameters = [
      'name' => $values['name'],
      'mail' => $values['mail'],
      'sport' => $term_sport->getName(),
    ];

    // Redirection.
    $form_state->setRedirect('sam.newsletters_form_redirection', $parameters);

    // Redirection using tempStore.
    // $this->store->set('newsletters_subscription_form_data', $parameters);
    // $form_state->setRedirect('sam.newsletters_form_redirection_ts');

    // TP 3 Formulaire - niveau 2 : gestion de l'utilisateur courant
    if (!$this->currentUser()->isAnonymous()) {
      $account = $this->entityTypeManager->getStorage('user')->load($this->currentUser()->id());
      $account->set('field_newsletter', ['value' => 1]);
      $account->save();
    }

    $this->messenger()->addStatus($this->t('Your subscription have been submitted'));
  }

}
