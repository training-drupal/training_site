<?php

namespace Drupal\sam\Form\TpBlock;

use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Class NewsletterSubscribeForm.
 */
class NewsletterSubscribeForm extends FormBase {

  /**
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * {@inheritdoc}
   */
  public function __construct(EntityTypeManagerInterface $entity_type_manager) {
    $this->entityTypeManager = $entity_type_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('entity_type.manager'),
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'block_newsletter_subscribe_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state, int $sport = NULL) {

    $form['name'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Your name'),
      '#required' =>TRUE,
    ];

    $form['mail'] = [
      '#type' => 'email',
      '#title' => $this->t('Your email address'),
      '#required' =>TRUE,
    ];

    if (!$this->currentUser()->isAnonymous()) {
      $account = $this->entityTypeManager->getStorage('user')->load($this->currentUser()->id());
      $form['name']['#default_value'] = $account->getAccountName();
      $form['mail']['#default_value'] = $account->getEmail();
    }

    $form['sport'] = [
      '#type' => 'entity_autocomplete',
      '#title' => $this->t('Center of interest'),
      '#required' => TRUE,
      '#target_type' => 'taxonomy_term',
      '#selection_settings' => [
        'target_bundles' => [
          'sport',
        ],
      ],
    ];

    if (!\is_null($sport)) {
      $form['sport']['#default_value'] = $this->entityTypeManager->getStorage('taxonomy_term')->load($sport);
      $form['sport']['#disabled'] = TRUE;
    }

    $form['postal_code'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Your postal code'),
      '#maxlength' => 5,
    ];

    $form['locality'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Your locality'),
    ];

    $form['actions']['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Submit'),
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {

    $postal_code = $form_state->getValue('postal_code');
    if ($postal_code != '') {

      if (\strlen($postal_code) < 5) {
        $form_state->setErrorByName('postal_code', $this->t('Enter a valid postal code !'));
      }
      if (!\preg_match('/^[0-9]*$/', $postal_code)) {
        $form_state->setErrorByName('postal_code', $this->t('Only number are allowed !'));
      }

      $locality = $form_state->getValue('locality');
      if ($locality == '') {
        $form_state->setErrorByName('locality', $this->t('You must provide your locality'));
      }
      if (\preg_match('/\d+/', $locality)) {
        $form_state->setErrorByName('locality', $this->t('Only character are allowed !'));
      }
    }
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $this->messenger()->addStatus($this->t('Your subscription have been submitted'));
  }
}
