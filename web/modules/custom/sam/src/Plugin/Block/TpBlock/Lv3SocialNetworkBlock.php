<?php

namespace Drupal\sam\Plugin\Block\TpBlock;

use Drupal\Core\Block\BlockBase;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Config\ConfigFactory;
use Drupal\Core\Cache\Cache;

/**
 * @Block(
 *  id = "block_tp_lv3",
 *  admin_label = @Translation("TP Block 3 : Social networks"),
 * )
 */
class Lv3SocialNetworkBlock extends BlockBase implements ContainerFactoryPluginInterface {

  /**
   * @var \Drupal\Core\Config\ConfigFactory
   */
  protected $config;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container,
    array $configuration,
    $plugin_id,
    $plugin_definition
  ) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('config.factory'),
    );
  }

  /**
   * {@inheritdoc}
   */
  public function __construct(array $configuration,
    $plugin_id,
    $plugin_definition,
    ConfigFactory $config
  ) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->config = $config;
  }

  /**
   * {@inheritdoc}
   */
  public function build() {
    $social_network_config = $this->config->get('sam.settings');
    return [
      '#theme' => 'block_socialnetwork',
      '#title' => $this->t('Social network'),
      '#config' => $social_network_config->getRawData(),
    ];
  }
}
