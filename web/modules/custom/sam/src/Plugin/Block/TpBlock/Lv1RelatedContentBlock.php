<?php

namespace Drupal\sam\Plugin\Block\TpBlock;

use Drupal\Core\Block\BlockBase;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Routing\RouteMatchInterface;
use Drupal\Core\Url;
use Drupal\Core\Utility\LinkGeneratorInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * @Block(
 *  id = "block_tp_lv1",
 *  admin_label = @Translation("TP Block 1 : Related contents to sports"),
 * )
 */
class Lv1RelatedContentBlock extends BlockBase implements ContainerFactoryPluginInterface {

  /**
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * @var \Drupal\Core\Utility\LinkGeneratorInterface
   */
  protected $linkGenerator;

  /**
   * @var \Drupal\Core\Routing\RouteMatchInterface
   */
  protected $routeMatch;


  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container,
    array $configuration,
    $plugin_id,
    $plugin_definition
  ) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('entity_type.manager'),
      $container->get('link_generator'),
      $container->get('current_route_match'),
    );
  }

  /**
   * {@inheritdoc}
   */
  public function __construct(array $configuration,
    $plugin_id,
    $plugin_definition,
    EntityTypeManagerInterface $entity_manager,
    LinkGeneratorInterface $link_generator,
    RouteMatchInterface $route_match
  ) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->entityTypeManager = $entity_manager;
    $this->linkGenerator = $link_generator;
    $this->routeMatch = $route_match;
  }

  /**
   * {@inheritdoc}
   */
  public function build() {

    /**
     * Prefer using Drupal\Core\Url rather than UrlGenerator service ...
     * @see https://api.drupal.org/api/drupal/core%21lib%21Drupal%21Core%21Routing%21UrlGeneratorInterface.php/function/UrlGeneratorInterface%3A%3AgenerateFromRoute/8.4.x
     */

    // Lists all 'sport' vocabulary terms.
    $list = [];
    $terms = $this->entityTypeManager->getStorage('taxonomy_term')->loadTree('sport');
    if (!empty($terms)) {
      foreach ($terms as $term) {
        $label = $this->t('Read more about') . ' ' . $term->name;
        $url = Url::fromRoute('entity.taxonomy_term.canonical', ['taxonomy_term' => $term->tid]);
        $list[] = $this->linkGenerator->generate($label, $url);
      }
    }

    // Pick only 'sport' term, related to the consulted article
    // if ($this->routeMatch->getRouteName() === 'entity.node.canonical') {
    //   $node = $this->routeMatch->getParameter('node');
    //   if ($node->bundle() != 'article') {
    //     return;
    //   }
    //   if (!$node->hasField('field_sport')) {
    //     return;
    //   }
    //   $field_sport = $node->get('field_sport')->first()->getValue();
    //   if (empty($field_sport)) {
    //     return;
    //   }
    //   $term = $this->entityTypeManager->getStorage('taxonomy_term')->load($field_sport['target_id']);
    //   $label = $this->t('Read more about') . ' ' . $term->getName();
    //   $url = Url::fromRoute('entity.taxonomy_term.canonical', ['taxonomy_term' => $field_sport['target_id']]);
    //   $list[] = $this->linkGenerator->generate($label, $url);
    // }

    return [
      '#theme' => 'item_list',
      '#items' => $list,
    ];
  }
}
