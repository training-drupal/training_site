<?php

namespace Drupal\sam\Plugin\Block\TpCache;

use Drupal\Core\Block\BlockBase;
use Drupal\Core\Cache\Cache;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Routing\RouteMatchInterface;
use Drupal\Core\Url;
use Drupal\Core\Utility\LinkGeneratorInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * @Block(
 *  id = "cache_tp",
 *  admin_label = @Translation("TP Cache : Related contents with cache management"),
 * )
 */
class RelatedContentCacheManagementBlock extends BlockBase implements ContainerFactoryPluginInterface {

  /**
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * @var \Drupal\Core\Utility\LinkGeneratorInterface
   */
  protected $linkGenerator;

  /**
   * @var \Drupal\Core\Routing\RouteMatchInterface
   */
  protected $routeMatch;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container,
    array $configuration,
    $plugin_id,
    $plugin_definition
  ) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('entity_type.manager'),
      $container->get('link_generator'),
      $container->get('current_route_match'),
    );
  }

  /**
   * {@inheritdoc}
   */
  public function __construct(array $configuration,
    $plugin_id,
    $plugin_definition,
    EntityTypeManagerInterface $entity_manager,
    LinkGeneratorInterface $link_generator,
    RouteMatchInterface $route_match
  ) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->entityTypeManager = $entity_manager;
    $this->linkGenerator = $link_generator;
    $this->routeMatch = $route_match;
  }

  /**
   * {@inheritdoc}
   */
  public function build() {

    if ($this->routeMatch->getRouteName() === 'entity.node.canonical') {
      $node = $this->routeMatch->getParameter('node');
      if ($node->bundle() != 'article') {
        return;
      }
      if (!$node->hasField('field_sport')) {
        return;
      }
      $field_sport = $node->get('field_sport')->first()->getValue();
      if (empty($field_sport)) {
        return;
      }
      $term = $this->entityTypeManager->getStorage('taxonomy_term')->load($field_sport['target_id']);
      $label = $this->t('Read more about') . ' ' . $term->getName();
      $url = Url::fromRoute('entity.taxonomy_term.canonical', ['taxonomy_term' => $field_sport['target_id']]);
      $list[] = $this->linkGenerator->generate($label, $url);
    }

    return [
      '#theme' => 'item_list',
      '#items' => $list,
    ];

    // return [
    //   '#theme' => 'item_list',
    //   '#items' => $list,
    //   '#cache' => [
    //     'tags' => ['node:' . $nid],
    //     'contexts' => ['url.path'],
    //   ]
    // ];
  }

  /**
   * {@inheritdoc}
   */
  public function getCacheTags() {
    $tags = [];
    if ($this->routeMatch->getRouteName() === 'entity.node.canonical') {
      $node = $this->routeMatch->getParameter('node');
      $tags = ['node:' . $node->id()];
    }
    return Cache::mergeTags(parent::getCacheTags(),$tags);
  }

  /**
   * {@inheritdoc}
   */
  public function getCacheContexts() {
    return Cache::mergeContexts(parent::getCacheContexts(), ['url.path']);
  }
}
