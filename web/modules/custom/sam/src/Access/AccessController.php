<?php

namespace Drupal\sam\Access;

use Drupal\Core\Access\AccessResult;
use Drupal\Core\Controller\ControllerBase;

/**
 * Defines AccessController class.
 */
class AccessController extends ControllerBase {

  public function checkAccess() {
    $roles = $this->currentUser()->getRoles();
    if (!in_array('administrator', $roles)) {
      return AccessResult::forbidden();
    }
    return AccessResult::allowed();
  }
}
